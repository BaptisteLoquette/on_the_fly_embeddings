import json
import pickle
import tqdm
from argparse import ArgumentParser
from sentence_transformers import SentenceTransformer
from blob_utils import set_up_blob_client, download_blob_to_file, upload_blob_file

parser  =   ArgumentParser(
    prog="embed.py",
    description="Downloads data from azure Blob Storage, embeds it and upload back to the blob"
    )
parser.add_argument('-connection_string', help="Azure Connexion str", type=str)

if __name__ == "__main__":
    args    =   parser.parse_args()
    filename        =   "data.json"
    out_filename    =   "results.json"
    connect_str     =   args.connection_string

    print("\n ---- \n", connect_str)

    
    blob_client =   set_up_blob_client(connection_string=connect_str)
    print("\n ---- \n")
    print("\t 1. Downloading data file from Azure Blob \n")
    
    download_blob_to_file(blob_service_client=blob_client, container_name="embedata", filename=filename)
    f = open(filename)
    data = json.load(f)

    model   =   SentenceTransformer('dangvantuan/sentence-camembert-large')

    results =   {
        "embed" : [],
        "id" : []
    }

    print("\n ---- \n")
    print("\t 2. Embedding text data \n")

    for content, id in tqdm.tqdm(zip(data['clean_content'], data['id']), total=len(data['clean_content'])):
        results['embed'].extend(model.encode([content]))
        results['id'].append(id)
    
    with open('results.json', 'wb') as f:
        pickle.dump(results, f)
    
    print("\n ---- \n")
    print("\t 3. Uploading results file to Azure Blob \n")
    upload_blob_file(blob_service_client=blob_client, container_name="embedata", filename=out_filename, blob=out_filename)
    print("\n ---- \n")
    print("\t 4. Files Uploaded !!")