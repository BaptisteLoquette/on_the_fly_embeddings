import os
from azure.storage.blob import BlobServiceClient

def set_up_blob_client(connection_string:str) -> BlobServiceClient:
    blob_service_client =   BlobServiceClient.from_connection_string(connection_string)
    return blob_service_client
    
def upload_blob_file(blob_service_client: BlobServiceClient, container_name: str, filename:str="data.json", blob:str="data.json") -> None:
    container_client = blob_service_client.get_container_client(container=container_name)
    with open(file=filename, mode="rb") as data:
        blob_client = container_client.upload_blob(name=blob, data=data, overwrite=True)


def download_blob_to_file(blob_service_client: BlobServiceClient, container_name, filename:str="results.json", blob:str="data.json"):
    blob_client = blob_service_client.get_blob_client(container=container_name, blob=blob)
    with open(file=filename, mode="wb") as sample_blob:
        download_stream = blob_client.download_blob()
        sample_blob.write(download_stream.readall())